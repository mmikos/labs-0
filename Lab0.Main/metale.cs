﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class metale : Pierwiastki
    {
        private int grupa { get; set; }

        public int Grupa { get { return grupa; } set { grupa = value; } }

        public metale()
            : base()
        {

        }

        public override string Opis()
        {

            return base.Opis() + string.Format(" z grupy {0}", Grupa);

        }

    }
}
