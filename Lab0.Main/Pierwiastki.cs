﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Pierwiastki
    {
        private string nazwa { get; set; }  
        private int liczba_atomowa { get; set; }

        public string Nazwa { get { return nazwa; } set { nazwa = value; } }
        public int Liczba_atomowa { get { return liczba_atomowa; } set { liczba_atomowa = value; } }

        public virtual string Opis()
        {
            return string.Format("{0} {1}", Nazwa, Liczba_atomowa);
        }
       
    }
}
