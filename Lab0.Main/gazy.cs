﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class gazy : Pierwiastki
    {
        private string rodzaj { get; set; }

        public string Rodzaj { get { return rodzaj; } set { rodzaj = value; } }

        public gazy()
            : base()
        {

        }

        public override string Opis()
        {
            return base.Opis() + string.Format(" jest to gaz {0}", Rodzaj);
        }
    }
}
