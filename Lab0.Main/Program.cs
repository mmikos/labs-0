﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Pierwiastki pierwiastek = new Pierwiastki();
            pierwiastek.Nazwa = "SN";
            pierwiastek.Liczba_atomowa = 53;

            metale metal = new metale();
            metal.Grupa = 7;
            metal.Liczba_atomowa = 22;
            metal.Nazwa = "PB";

            gazy gaz = new gazy();
            gaz.Liczba_atomowa = 45;
            gaz.Nazwa = "GG";
            gaz.Rodzaj = "Zlozony";

            Console.WriteLine(pierwiastek.Opis());
            Console.WriteLine(metal.Opis());
            Console.WriteLine(gaz.Opis());
            Console.ReadKey();
        }
    }
}
